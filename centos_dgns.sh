#!/bin/bash

echo -e "\nAll the paths you enter in this script must NOT end with a forward slash '/'"
echo -e "\nThis script MUST be executed with the ROOT user.\n"

on_centos=false
echo "Select the number of the OS you are on."
select yn in "CentOS" "Buster"; do
    case $yn in
        CentOS ) on_centos=true; break;;
        Buster ) break;;
    esac
done

# If we are on CentOS then we need to run the supervisor command little differently
if $on_centos; then
	echo
	read -p "Enter the ip of the server: " server_ip
fi

echo
read -p "Enter the user that the process will use (if the user don't exists will be created): " user
echo
read -p "Enter the group that the process will use (if the group don't exists will be created): " group
echo
read -p "Enter a name for the process of the app (supervisor): " supervisor_app_name
echo
read -p "Enter the full path of the ROOT of the project where all the required directories will be created (if they don't exist): " root_dir_proj_path
echo
read -p "Enter the relative path from the ROOT directory -> ${root_dir_proj_path}/ to the django project directory (e.g my_django_dir): " django_project_dir_rel_path
echo
read -p "Enter the name of the django project directory inside the -> ${root_dir_proj_path}/${django_project_dir_rel_path}/ (e.g. my_django_project): " django_project_name
echo
read -p "Enter the full path of the project's virtual environment (if the venv doesn't exists will be created e.g. /webapps/venvs/my_venv), leave empty to create this venv -> $root_dir_proj_path/venv : " project_venv
echo
read -p "Enter the full path of the supervisor configuration file to be created/overrided (e.g. /etc/supervisor/conf.d/${supervisor_app_name}.conf OR /etc/supervisord.d/${supervisor_app_name}.conf): " supervisor_conf_path
echo
read -p "Enter the domain name (without www): " serveripordomain
echo


create_alias=false
echo "You want to create the alias 'cdto${supervisor_app_name}' in /root/.bash_profile for easy acces of the project ROOT dir?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) create_alias=true; break;;
        No ) break;;
    esac
done

echo

echo
read -p "You want to specify the run directory where the gunicorn_start script will be located? (leave empty for default -> ${root_dir_proj_path}/run): " run_dir
echo

echo "You want to generate a replace_project script?"
create_replace_project_script=false
select yn in "Yes" "No"; do
    case $yn in
        Yes ) read -p "Enter the path for the replace_project.sh to be created/overrided or leave empty to create/override this file -> ${root_dir_proj_path}/replace_project.sh: " replace_project_script_path;create_replace_project_script=true; break;;
        No ) break;;
    esac
done

echo
read -p "Enter the media directory of django or leave empty to create/use this -> ${root_dir_proj_path}/${supervisor_app_name}_media/: " media_dir_path

echo
read -p "Enter the static directory of django or leave empty to create/use this -> ${root_dir_proj_path}/${supervisor_app_name}_static/: " static_dir_path

echo -e "\nYou want to generate a undo_everything.sh script? (for script testing and development purposes)"
create_undone_everything_script=false
select yn in "Yes" "No"; do
    case $yn in
        Yes ) create_undone_everything_script=true; break;;
        No ) break;;
    esac
done


# Setting some variables that are different between OS's
# If we are on CentOS then we need to run the supervisor command little differently
if $on_centos; then
	# If we are on centos the run the supervisor command with the -c argument fix.	
	supervisor_cmd_restart="supervisorctl -c /etc/supervisord.conf restart ${supervisor_app_name}"
	supervisor_cmd_status="supervisorctl -c /etc/supervisord.conf status ${supervisor_app_name}"
else
	supervisor_cmd_restart="supervisorctl restart ${supervisor_app_name}"
	supervisor_cmd_status="supervisorctl status ${supervisor_app_name}"
fi

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

echo -e "\nCreating necessary dirs (if they don't exist)...\n"
djangod_dir_full_path=${root_dir_proj_path}/${django_project_dir_rel_path}

# If the variable 'media_dir_path' its empty then initialize it with the default
if [ -z "$media_dir_path" ]; then
	media_dir_path=${root_dir_proj_path}/${supervisor_app_name}_media
fi
media_dir_path=${media_dir_path}/

# If the variable 'static_dir_path' its empty then initialize it with the default
if [ -z "$static_dir_path" ]; then
	static_dir_path=${root_dir_proj_path}/${supervisor_app_name}_static
fi
static_dir_path=${static_dir_path}/

# If the variable 'run_dir' its empty then initialize it with the default
if [ -z "$run_dir" ]; then
	run_dir=${root_dir_proj_path}/run
fi
logs_dir=${root_dir_proj_path}/logs


if [ ! -d "$run_dir" ]; then
	echo "Creating directory -> ${run_dir}"
	mkdir ${run_dir}
else
	echo "Already exists so not created: ${run_dir}"
fi

if [ ! -d "$logs_dir" ]; then
	echo "Creating directory -> ${logs_dir}"
	mkdir ${logs_dir}
else
	echo "Already exists so not created: ${logs_dir}"
fi
if [ ! -d "$static_dir_path" ]; then
	echo "Creating directory -> ${static_dir_path}"
	mkdir ${static_dir_path}
else
	echo "Already exists so not created: ${static_dir_path}"
fi
if [ ! -d "$media_dir_path" ]; then
	echo "Creating directory -> ${media_dir_path}"
	mkdir ${media_dir_path}
else
	echo "Already exists so not created: ${media_dir_path}"
fi

echo -e "\nDone.\n"


echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

# Creating user and group if don't exists
echo "Creating user and group if don't exists..."
groupadd --system ${group}
useradd --system --gid ${group} --shell /bin/bash --home ${root_dir_proj_path} ${user}
#

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

# Execute chown of the entire ROOT directory recursively.
echo "Executing chown of the entire ROOT directory recursively as ${user}:${group}"
chown $user:$group ${root_dir_proj_path} -R

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'


if [ -z "$project_venv" ]; then 
	# If the variable 'project_venv' its empty then initialize the default.
	project_venv="$root_dir_proj_path/venv"
fi
if [ ! -d "$project_venv" ]; then
	echo -e "\nVirtual enviroment doesn't exists, so creating new venv with python3.8 as ${user}\n\n"
	# If we are on centos then create venv with python3.8 else with 3.7
	if $on_centos; then
		su - $user -c "python3.8 -m venv $project_venv"
	else
		su - $user -c "python3.7 -m venv $project_venv"
	fi
	su - $user -c "source ${project_venv}/bin/activate && pip install --upgrade pip && pip install -r ${djangod_dir_full_path}/requirements.txt"
else
	echo -e "\nVirtual enviroment exists so not creating...\n"
fi


echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'


echo -e '\nCreating Gunicorn Script...\n\n'
sock_file_path=${run_dir}/gunicorn.sock
gunicorn_start_script=${run_dir}/gunicorn_start
touch ${gunicorn_start_script} && chmod +x ${gunicorn_start_script}
cat << EOF | tee ${gunicorn_start_script}
#!/bin/bash
NAME="${supervisor_app_name}"
DJANGODIR=${djangod_dir_full_path}
SOCKFILE=${sock_file_path}
USER=$user
GROUP=$group
NUM_WORKERS=3
DJANGO_SETTINGS_MODULE=${django_project_name}.settings
DJANGO_WSGI_MODULE=${django_project_name}.wsgi

# Activate the virtual environment
cd \$DJANGODIR
source ${project_venv}/bin/activate
export DJANGO_SETTINGS_MODULE=\$DJANGO_SETTINGS_MODULE
export PYTHONPATH=\$DJANGODIR:\$PYTHONPATH

exec ${project_venv}/bin/gunicorn \${DJANGO_WSGI_MODULE}:application \
  --name \$NAME \
  --workers \$NUM_WORKERS \
  --user=\$USER --group=\$GROUP \
  --bind=unix:\$SOCKFILE \
  --log-level=debug \
  --log-file=-
EOF
echo -e "\n\nCreated gunicorn start script at -> ${gunicorn_start_script}\n" 


echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'


echo -e '\nCreating Supervisor Configuration File...\n'
supervisor_logfile=${logs_dir}/gunicorn_supervisor.log
touch $supervisor_conf_path
cat << EOF | tee $supervisor_conf_path
[program:${supervisor_app_name}]
command = ${gunicorn_start_script}
user = $user
stdout_logfile = ${supervisor_logfile}
redirect_stderr = true
environment=LANG=en_US.UTF-8,LC_ALL=en_US.UTF-8
EOF
touch ${supervisor_logfile}
echo -e "\n"
if $on_centos; then
	supervisorctl -c /etc/supervisord.conf reread
	supervisorctl -c /etc/supervisord.conf update
else
	supervisorctl reread
	supervisorctl update
fi
echo -e "\n\nCreated supervisor configuration file at -> ${supervisor_conf_path}\n" 


echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

echo -e '\nCreating Nginx Configuration File...\n'
if $on_centos; then
	nginx_conf_file_path="/etc/nginx/conf.d/vhosts/${serveripordomain}.conf"
	# If we are on CentOS then use the ip and the port else only the port
	nginx_listen_80="${server_ip}:80"
	nginx_listen_443="${server_ip}:443"
else
	nginx_conf_file_path="/etc/nginx/sites-available/${serveripordomain}.conf"
	nginx_listen_80=80
	nginx_listen_443=443
fi
touch ${nginx_conf_file_path}

if $on_centos ; then
echo -e "Creating nginx conf with SSL for CentOS (the only difference it's on the location of the SSL certificate)...\n\n"
cat << EOF | tee ${nginx_conf_file_path}
# Redirect HTTP -> HTTPS
server {
	listen ${nginx_listen_80};
	server_name $serveripordomain www.$serveripordomain;
	return 301 https://$serveripordomain\$request_uri;
}

# Redirect WWW -> NON-WWW
server {
	listen ${nginx_listen_443} ssl;
	server_name www.$serveripordomain;
	
	#ssl related stuff...
	ssl_certificate      /etc/pki/tls/certs/${serveripordomain}.bundle;
	ssl_certificate_key  /etc/pki/tls/private/${serveripordomain}.key;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA!RC4:EECDH:!RC4:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS;
	ssl_prefer_server_ciphers   on;
	ssl_session_cache   shared:SSL:10m;
	ssl_session_timeout 60m;
	
	return 301 https://$serveripordomain\$request_uri;
}

server {
	listen ${nginx_listen_443} ssl;
	server_name $serveripordomain;
	
	#ssl related stuff...
	ssl_certificate      /etc/pki/tls/certs/${serveripordomain}.bundle;
	ssl_certificate_key  /etc/pki/tls/private/${serveripordomain}.key;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256:EECDH+aRSA!RC4:EECDH:!RC4:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS;
	ssl_prefer_server_ciphers   on;
	ssl_session_cache   shared:SSL:10m;
	ssl_session_timeout 60m;

	access_log ${logs_dir}/nginx-access.log;
	error_log ${logs_dir}/nginx-error.log;
 
	location /static/ {
		alias   ${static_dir_path};
	}
    
	location /media/ {
		alias   ${media_dir_path};
	}

	location / {    	
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
		
        # enable this if and only if you use HTTPS, this helps Rack
        # set the proper protocol for doing redirects:
        proxy_set_header X-Forwarded-Proto https;

        # pass the Host: header from the client right along so redirects
        # can be set properly within the Rack application (django)
        proxy_set_header Host \$http_host;

        # we don't want nginx trying to do something clever with
        # redirects, we set the Host: header above already.
        proxy_redirect off;

		proxy_pass http://unix:${sock_file_path};
	}	

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}
}
EOF

else

echo -e "Creating nginx conf with SSL for Buster (the only difference it's on the location of the SSL certificate)...\n\n"
cat << EOF | tee ${nginx_conf_file_path}
# Redirect HTTP -> HTTPS
server {
	listen ${nginx_listen_80};
	server_name $serveripordomain www.$serveripordomain;
	return 301 https://$serveripordomain\$request_uri;
}

# Redirect WWW -> NON-WWW
server {
	listen ${nginx_listen_443} ssl;
	server_name www.$serveripordomain;
	
	#ssl related stuff...
	ssl_certificate /etc/letsencrypt/live/${serveripordomain}/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/${serveripordomain}/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

	return 301 https://$serveripordomain\$request_uri;
}

server {
	listen ${nginx_listen_443} ssl;
	server_name $serveripordomain;
	
	#ssl related stuff...
	ssl_certificate /etc/letsencrypt/live/${serveripordomain}/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/${serveripordomain}/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

	access_log ${logs_dir}/nginx-access.log;
	error_log ${logs_dir}/nginx-error.log;
 
	location /static/ {
		alias   ${static_dir_path};
	}
    
	location /media/ {
		alias   ${media_dir_path};
	}

	location / {    	
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
		
        # enable this if and only if you use HTTPS, this helps Rack
        # set the proper protocol for doing redirects:
        proxy_set_header X-Forwarded-Proto https;

        # pass the Host: header from the client right along so redirects
        # can be set properly within the Rack application (django)
        proxy_set_header Host \$http_host;

        # we don't want nginx trying to do something clever with
        # redirects, we set the Host: header above already.
        proxy_redirect off;

		proxy_pass http://unix:${sock_file_path};
	}	

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}
}
EOF

fi
echo -e "\nCreated nginx configuration file at -> ${nginx_conf_file_path}\n" 

if $on_centos; then
	echo
else
	echo "We are NOT on CentOS so we are creating a symlink to the sites-enabled of nginx"
	ln -s /etc/nginx/sites-available/${serveripordomain}.conf /etc/nginx/sites-enabled/${serveripordomain}.conf
fi

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

# If the variable 'create_replace_project_script' is equals to true only then create the script
django_settings_path=${djangod_dir_full_path}/${django_project_name}/settings.py
if $create_replace_project_script; then
# If the variable 'replace_project_script_path' its empty then initialize the default.
if [ -z "$replace_project_script_path" ]; then
	replace_project_script_path=${root_dir_proj_path}/replace_project.sh
fi
echo -e "\nCreating 'replaceproj.sh' script for project replacement from git.\n\n"
touch $replace_project_script_path
cat << EOF | tee $replace_project_script_path
#!/bin/bash
django_settings_path=${django_settings_path}
username=${user}

cd ${djangod_dir_full_path}
sudo -H -u \$username bash -c "git fetch --all && git reset --hard origin/master"

sudo -H -u \$username bash -c "sed -i \"s/DEBUG = True/DEBUG = False/\" \${django_settings_path}"
sudo -H -u \$username bash -c "echo \"ALLOWED_HOSTS = ['${serveripordomain}']\" >> \${django_settings_path}"

# Delete any MEDIA_ROOT line & append the correct one
sudo -H -u \$username bash -c "sed -i \"s/MEDIA_ROOT =.*//\" \${django_settings_path}"
sudo -H -u \$username bash -c "echo \"MEDIA_ROOT = '${media_dir_path}'\" >> \${django_settings_path}"

# Delte any STATIC_ROOT line & append the correct one
sudo -H -u \$username bash -c "sed -i \"s/STATIC_ROOT =.*//\" \${django_settings_path}"
sudo -H -u \$username bash -c "echo \"STATIC_ROOT = '${static_dir_path}'\" >> \${django_settings_path}"

# Collect static
sudo -H -u \$username bash -c "source ${project_venv}/bin/activate && python ${djangod_dir_full_path}/manage.py collectstatic --noinput"

echo -e "\n*** In order for the changes to take effect you must restart the supervisor process and the nginx server with root privileges. ***\n"

EOF

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'
fi

if $create_alias; then
	echo "alias cdto${supervisor_app_name}='cd ${root_dir_proj_path}'" >> /root/.bash_profile
	echo "Created alias -> cdto${supervisor_app_name}='cd ${root_dir_proj_path}'"
	echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'
fi

echo -e "Fixing permissions recursively -> \n\n1) chown everything to ${user}:${group}. \n\n2) chmod files to 644 and folders to 755."
find $root_dir_proj_path -type d -exec chmod 755 {} \;  # Change directory permissions rwxr-xr-x
find $root_dir_proj_path -type f -exec chmod 644 {} \;  # Change file permissions rw-r--r--
chown ${user}:${group} -R $root_dir_proj_path

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

# After the permisions was reseted we must make the executable scripts, executables again.
echo -e "Making gunicorn_start script executable with chmod +x"
chmod +x $run_dir/gunicorn_start

echo -e "\nMaking venv/bin/gunicorn executable with chmod +x"
chmod +x $project_venv/bin/gunicorn

echo -e "\nMaking venv/bin/pip executable with chmod +x"
chmod +x $project_venv/bin/pip

# If the variable 'create_replace_project_script' is equals to true its means that a script was created so make it executable.
if $create_replace_project_script; then
	echo -e "\nMaking the replace_project.sh executable with chmod +x"
	chmod +x $replace_project_script_path
fi

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

echo "Creating alias in /root/.bash_profile to restart the supervisor process & nginx with one command line\n"
echo "alias restart_${supervisor_app_name}='${supervisor_cmd_restart}'" >> /root/.bash_profile
echo -e "\nAlias created -> restart_${supervisor_app_name}='${supervisor_cmd_restart}'"



echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

if $create_undone_everything_script; then
echo -e "\nCreating 'undo_everything.sh' script for script testing and development purposes.\n\n"
create_undone_everything_script_path=${root_dir_proj_path}/undo_everything.sh
touch $create_undone_everything_script_path && chmod +x $create_undone_everything_script_path && chown $user:$group $create_undone_everything_script_path
cat << EOF | tee $create_undone_everything_script_path
#!/bin/bash
rm -rf $media_dir_path $static_dir_path $run_dir $logs_dir $project_venv $replace_project_script_path $nginx_conf_file_path $supervisor_conf_path
EOF

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'
fi

# Restart app and server
echo -e "\n1) Restarting supervisor process.\n"
${supervisor_cmd_restart}
echo -e "\n2) Running status on the supervisor process.\n"
${supervisor_cmd_status}
echo -e "\n3) Running nginx -t to see if everything it's ok...\n"
nginx -t
echo -e "\n*** If the syntax is ok and the test was successful go ahead and restart the nginx server with 'service nginx restart'.\n"

echo -e '\n---------------------------------------------------------------------------------------------------------------------------------------------------\n'

echo -e "*********** FINAL THINGS YOU MAYBE WANT TO DO ***********\n"
echo -e "\n*** Execute the replace_project.sh as the user -> ${user} ***"
echo -e "\n*** Activate the venv as the user -> ${user} and migrate the migrations to the database with 'python manage.py migrate' ***"
echo -e "\n*********************************************************\n\n"
